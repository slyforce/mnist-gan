import tensorflow as tf

from utils import get_nr_variables

def fully_connected(scope, x, n_out, reuse, use_relu=True, name='linear/'):
  relu_initializer = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False, seed=None,
                                                                    dtype=tf.float32)
  with tf.variable_scope(scope, reuse=reuse):
        x = tf.layers.dense(x, n_out, kernel_initializer=relu_initializer if use_relu else None ,
                            name=name+"-weights", use_bias=True)
        if use_relu:
          x = tf.nn.leaky_relu(x, 0.2)

  return x

def GeneratorFFNN_cGan(z_dim, batch_size, classes, is_train,
                       reuse=False, n_layers=2, h_dim=100, output_dim=(28,28,1)):
    with tf.variable_scope("Generator", reuse=reuse) as vs:

        # generate gaussian noise
        z = tf.random_normal(stddev=1., shape=[batch_size, z_dim], name='g_noise')

        # project the one-hot vector classes to the hidden dimension
        classes = fully_connected(vs, classes, h_dim, reuse, use_relu=False, name="g_classes")
        classes = tf.nn.tanh(classes)

        # concatenate the noise and class embeddings
        # input to the first hidden layer has a dimension of h_dim + z_dim
        z = tf.concat([z, classes], axis=-1)

        # build an MLP consisting of multiple feed-forward layers with relu activations
        for i in range(0, n_layers):
            z = fully_connected(vs, z, h_dim, reuse, name="G-layer-relu-" + str(i))

        # final linear transformation to the number of features in the output shape
        out_x = fully_connected(vs, z, output_dim[0] * output_dim[1] * output_dim[2],
                                reuse, use_relu=False, name="output")
        out_x = tf.nn.tanh(out_x)

        # reshape to the specified shape
        out_x = tf.reshape(out_x, [batch_size] + list(output_dim))

        # log the output
        if reuse == False:
          tf.summary.image('out_x', out_x)

    # output # of variables
    variables = tf.contrib.framework.get_variables(vs)
    if not reuse:
        print "Generator variables:", get_nr_variables(variables)

    return out_x, variables

def DiscriminatorFFNN_cGan(x, x_classes, x_G, x_G_classes, n_layers=2, h_dim=100):
    with tf.variable_scope("Discriminator") as vs:
        B = tf.shape(x)[0]
        # tf.summary.image('discriminator real input', x)
        # tf.summary.image('discriminator fake input', x_G)

        # concatenate both generated and real samples into one batch
        x = tf.concat([x, x_G], 0)
        x_classes = tf.concat([x_classes, x_G_classes], 0)

        # flatten the 2d image input
        x = tf.squeeze(x, [3])
        x = tf.reshape(x, (2*B, x.shape[1] * x.shape[2]))

        # project the one-hot vector classes to the hidden dimension
        x_classes = fully_connected(vs, x_classes, h_dim, False, False, name="d_classes")
        x_classes = tf.nn.tanh(x_classes)

        # concatenate input images with their classes
        x = tf.concat([x, x_classes], axis=-1)

        for i in range(0, n_layers):
            x = fully_connected(vs, x, h_dim, False, name='D-linear-'+str(i))

        # binary prediction
        prediction = tf.layers.dense(x, 1, activation=tf.nn.sigmoid)

        out_x = prediction[0:B, :]
        out_x_G = prediction[B:, :]

    variables = tf.contrib.framework.get_variables(vs)
    print "Discriminator variables:", get_nr_variables(variables)
    return out_x, out_x_G, variables
