import tensorflow as tf

def conv2d(scope, x, filters=64, kernel_size=2, name='conv-2d'):
    with tf.variable_scope(scope):
        x = tf.layers.conv2d(x, filters=filters, kernel_size=kernel_size, strides=(2, 2), name=name+"-conv1")
        x = tf.layers.average_pooling2d(x, kernel_size, strides=(1,1))
    return x

def fully_connected(scope, x, n_out, reuse, is_train=False, activation=None, name='linear/'):
    with tf.variable_scope(scope, reuse=reuse):
        x = tf.layers.dense(x, n_out, activation=activation, name=name+"-weights", use_bias=True)
        #x = tf.contrib.layers.batch_norm(x, center=True, scale=True, is_training=is_train, scope=name+'-bn')
    return x

def get_nr_variables(vars):
    tot_nb_params = 0
    for v in vars:
        shape = v.get_shape() # e.g [D,F] or [W,H,C]
        params = 1
        for dim in range(0, len(shape)):
            params *= int(shape[dim])

        tot_nb_params += params

    return tot_nb_params

def GeneratorFFNN(z_dim, batch_size, is_train, reuse=False, n_layers=2, h_dim=100, output_dim=(28,28,1)):
    with tf.variable_scope("Generator", reuse=reuse) as vs:
        z = tf.random_normal(mean=0., stddev=1., shape=[batch_size, z_dim], name='g_noise') # random gaussian sample of z_dim

        for i in range(0, n_layers):
            z = fully_connected(vs, z, h_dim, reuse, is_train=is_train, activation=None, name="G-layer-relu-" + str(i))
            z = tf.nn.leaky_relu(z, 0.2)

        out_x = fully_connected(vs, z, output_dim[0] * output_dim[1] *  output_dim[2] , reuse, is_train=is_train, activation=tf.nn.tanh, name="output")
        out_x = tf.reshape(out_x, [batch_size] + list(output_dim))
        if reuse == False:
          tf.summary.image('out_x', out_x)

    variables = tf.contrib.framework.get_variables(vs)

    if not reuse:
        print "Generator variables:", get_nr_variables(variables)

    return out_x, variables

def DiscriminatorFFNN(x, x_G, n_layers=2, h_dim=100, dropout_rate=0.2):
    with tf.variable_scope("Discriminator") as vs:
        B = tf.shape(x)[0]

        x = tf.concat([x, x_G], 0)
        x = tf.squeeze(x, [3]) # squeeze the channel dimension
        x = tf.reshape(x, (2*B, x.shape[1] * x.shape[2])) # collapse the two dimensions into 1

        for i in range(0, n_layers):
            x = fully_connected(vs, x,  h_dim, False, False, activation=None, name='D-linear-'+str(i))
            x = tf.nn.leaky_relu(x, 0.2)
            #x = tf.layers.dropout(x, rate=dropout_rate, training=True)

        prediction = tf.layers.dense(x, 1, activation=tf.nn.sigmoid)

        out_x = prediction[0:B, :]
        out_x_G = prediction[B:, :]

    variables = tf.contrib.framework.get_variables(vs)
    print "Discriminator variables:", get_nr_variables(variables)
    return out_x, out_x_G, variables
