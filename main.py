import tensorflow as tf
from config import get_config
from trainer import Trainer
from data import MNISTData, CustomData
import os

sess = tf.Session()
config, _ = get_config()

if config.mnist == True:
    data = MNISTData()
else:
    data = CustomData('data/' + config.dataset, config.height, config.width, config.channels)

trainer = Trainer(config, data)

try:
    os.mkdir('outputs')
except OSError:
    pass

if config.debug == True:
    assert(config.batch_size == 1)
    trainer.train_test()
else:
    trainer.train()
