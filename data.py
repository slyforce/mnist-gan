from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as np

from utils import save_image

import os
from PIL import Image

class Data:
    def train_batch(self):
        pass

    def get_dimensions(self):
        pass

class MNISTData(Data):
    def __init__(self):
        self.mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

        self.x = tf.placeholder(dtype=tf.float32, shape=[None, 28, 28, 1], name='data_input')
        self.y = tf.placeholder(dtype=tf.float32, shape=[None, 10], name='class_input')

        self.test_image, self.test_class = self.mnist.train.next_batch(1)
        self.test_image = np.reshape(self.test_image, newshape=(1, 28, 28, 1))

    def train_batch(self, batch_size):
        x, y = self.mnist.train.next_batch(batch_size)
        return np.reshape(x, newshape=(batch_size, 28, 28, 1)), y

    def test_data(self):
        return self.test_image, self.test_class

    def get_dimensions(self):
        return (28, 28, 1)

class CustomData(Data):
    def __init__(self, path, height, width, channels):
        self.path = path
        self.dims = (height, width, channels)

        self.read_data(path)
        self.x = tf.placeholder(dtype=tf.float32, shape=[None] + list(self.dims), name='data_input')

        self.iteration_counter = 0

    def read_data(self, path):
        self.images = []
        for file in os.listdir(path):
            abs_path = os.path.abspath(path) + '/' + file
            self.images.append(self.read_image(abs_path))

        self.images = np.array(self.images)

        print self.images.shape

    def read_image(self, file):
        im = Image.open(file)
        im = im.resize((self.dims[0], self.dims[1]), Image.ANTIALIAS)

        im_arr = np.fromstring(im.tobytes(), dtype=np.uint8)
        im_arr = im_arr.reshape((im.size[0], im.size[1], 3))

        return im_arr

    def get_dimensions(self):
        return self.dims

    def train_batch(self, batch_size):
        if batch_size + self.iteration_counter >= self.images.shape[0]:
            #TODO: This doesn't work properly, check how mnist_data in tensorflow parses the dataset
            # or think properly
            raise NotImplementedError

            remainder = batch_size + self.iteration_counter - self.images.shape[0]
            batch = np.vstack((self.images[self.iteration_counter:, :,:,:], self.images[0:remainder, :, :, :]))
            self.iteration_counter = remainder
        else:
            batch = self.images[self.iteration_counter:self.iteration_counter+batch_size, :,:,:]
            self.iteration_counter += batch_size

        return batch
