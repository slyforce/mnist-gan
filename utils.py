import matplotlib.pyplot as plt
from matplotlib import image
import numpy as np

def save_image(array, filename):
    if not filename.endswith(".png"):
        filename += ".png"

    try:
        image.imsave(filename, array)
    except:
        print "Cannot save image of shape", array.shape



def compare_images(x, y):
    fig = plt.figure()
    a = fig.add_subplot(1, 2, 1)
    imgplot = plt.imshow(x)
    a = fig.add_subplot(1, 2, 2)
    imgplot = plt.imshow(y)
    plt.show()


def get_nr_variables(vars):
    tot_nb_params = 0
    for v in vars:
        shape = v.get_shape() # e.g [D,F] or [W,H,C]
        params = 1
        for dim in range(0, len(shape)):
            params *= int(shape[dim])

        tot_nb_params += params

    return tot_nb_params