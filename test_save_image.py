from tensorflow.examples.tutorials.mnist import input_data
from utils import save_image
import numpy as np

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

img, _ = mnist.train.next_batch(1)
img = np.reshape(img, newshape=(28,28))

save_image(img, 'test_output.png')
