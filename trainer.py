import tensorflow as tf
from model import GeneratorFFNN, DiscriminatorFFNN
from model_cgan import GeneratorFFNN_cGan, DiscriminatorFFNN_cGan
import numpy as np
from utils import save_image

class Trainer:
    def __init__(self, config, data_loader):
        self.config = config
        self.data_loader = data_loader

        self.dims = data_loader.get_dimensions()
        self.batch_size = config.batch_size
        self.z_dim = config.z_dim
        self.G_h_dim = config.G_h_dim
        self.is_train = config.is_train

        self.D_lr = config.d_lr
        self.G_lr = config.g_lr

        self.use_cgan = config.use_cgan

        self.build_models()

        self.step = tf.Variable(0, name='step', trainable=False)
        self.step_increment = tf.assign(self.step, self.step + 1)

        self.max_step = config.max_step
        self.log_step = config.log_step
        self.model_dir = config.model_dir
        self.load_path = config.load_path

        self.saver = tf.train.Saver()
        self.summary_writer = tf.summary.FileWriter(self.model_dir)

        tf.summary.scalar("d_loss", self.D_loss)
        tf.summary.scalar("g_loss", self.G_loss)
        self.summary_op = tf.summary.merge_all()

        sv = tf.train.Supervisor(logdir=self.model_dir,
                                 is_chief=True,
                                 saver=self.saver,
                                 summary_op=None,
                                 summary_writer=self.summary_writer,
                                 save_model_secs=300,
                                 global_step=self.step,
                                 ready_for_local_init_op=None)

        gpu_options = tf.GPUOptions(allow_growth=True)
        sess_config = tf.ConfigProto(allow_soft_placement=True,
                                     gpu_options=gpu_options)

        self.sess = sv.prepare_or_wait_for_session(config=sess_config)

    def build_models(self):
        self.x = self.data_loader.x
        self.y = self.data_loader.y

        if self.use_cgan:
            self.build_cgan_models()
        else:
            self.build_gan_models()

        self.D_loss = - tf.reduce_mean( tf.log(self.D) + tf.log(1 - self.D_G) )
        self.G_loss = - tf.reduce_mean( tf.log(self.D_G) ) # as said in https://github.com/soumith/ganhacks

        if self.is_train:
            self.D_train_step = tf.train.AdamOptimizer(learning_rate=self.D_lr).minimize(self.D_loss, var_list=[self.vars_D])
            self.G_train_step = tf.train.AdamOptimizer(learning_rate=self.G_lr).minimize(self.G_loss, var_list=[self.vars_G])

    def build_cgan_models(self):
        print "Using cGan"
        self.G, self.vars_G = GeneratorFFNN_cGan(self.z_dim, self.batch_size, self.y,
                                            True,
                                            h_dim=self.G_h_dim,
                                            n_layers=self.config.n_layers_G,
                                            output_dim=self.dims,
                                            reuse=False)

        self.G_test, _ = GeneratorFFNN_cGan(self.z_dim, self.batch_size, self.y,
                                       False,
                                       h_dim=self.G_h_dim,
                                       n_layers=self.config.n_layers_G,
                                       output_dim=self.dims,
                                       reuse=True)

        self.D, self.D_G, self.vars_D = DiscriminatorFFNN_cGan(self.x,
                                                          self.y,
                                                          self.G,
                                                          self.y,
                                                          n_layers=self.config.n_layers_D,
                                                          h_dim=self.config.D_h_dim)

    def build_gan_models(self):
        self.G, self.vars_G = GeneratorFFNN(self.z_dim, self.batch_size, True,
                                            h_dim=self.G_h_dim,
                                            n_layers=self.config.n_layers_G,
                                            output_dim=self.dims,
                                            reuse=False)
        self.G_test, _ = GeneratorFFNN(self.z_dim, self.batch_size, False,
                                       h_dim=self.G_h_dim,
                                       n_layers=self.config.n_layers_G,
                                       output_dim=self.dims,
                                       reuse=True)

        self.D, self.D_G, self.vars_D = DiscriminatorFFNN(self.x,
                                                          self.G,
                                                          n_layers=self.config.n_layers_D,
                                                          h_dim=self.config.D_h_dim)

    def generate_tile_of_1D_images(self):

        # one-hot encoding of random classes
        random_classes = np.random.randint(0, 10, size=(self.batch_size))
        one_hot_input = np.zeros((self.batch_size, 10))
        one_hot_input[np.arange(self.batch_size), random_classes] = 1

        feed_dict = {self.y : one_hot_input}
        G = self.sess.run(self.G_test, feed_dict)

        G = G[:, :, :, 0]

        self.print_pairwise_statistics(G)

        n_cols = 8
        n_rows = self.batch_size // n_cols

        dims = self.data_loader.get_dimensions()

        array = np.zeros((dims[0] * n_cols, dims[1] * n_rows))
        for i in range(0, n_cols):
            for j in range(0, n_rows):
                array[i*dims[0]:(i+1)*dims[0], j*dims[1]:(j+1)*dims[1]] = G[i*n_rows+j, :, :]

        return array

    def generate_tile_of_3D_images(self):
        G = self.sess.run(self.G_test)

        if self.batch_size < 8:
            n_cols = 1
        else:
            n_cols = 8
        n_rows = self.batch_size // n_cols

        dims = self.data_loader.get_dimensions()
        array = np.zeros((dims[0] * n_cols, dims[1] * n_rows, 3))
        for i in range(0, n_cols):
            for j in range(0, n_rows):
                array[i*dims[0]:(i+1)*dims[0], j*dims[1]:(j+1)*dims[1], :] = G[i*n_rows+j, :, :]

        return array

    def print_pairwise_statistics(self, G):
        differences = []
        for i in range(1, G.shape[0]):
            differences.append(np.sum(abs(G[i - 1, :, :] - G[i, :, :])))
        print "(max/avg/min) pixel-wise difference between generated samples", np.max(differences), np.mean(differences), np.min(
            differences)

    def train(self):
        for iteration in range(0, self.max_step):
            fetch_list = [self.summary_op, self.D_train_step, self.G_train_step,
                          self.G_loss, self.D_loss, self.G, self.D, self.step_increment]

            x_in, y_in = self.data_loader.train_batch(self.batch_size)
            feed_dict = {self.x : x_in, self.y : y_in }
            summary, _, _, g_loss, d_loss, G, D, _ = self.sess.run(fetch_list, feed_dict)

            self.summary_writer.add_summary(summary, self.step.eval(session=self.sess))

            if iteration % self.log_step == 0:
                print "G_loss: ", g_loss
                print "D_loss: ", d_loss
                print "--------------------------"

                if self.dims[2] == 1:
                    img = self.generate_tile_of_1D_images()
                else:
                    img = self.generate_tile_of_3D_images()

                save_image(img, './outputs/out-' + str(iteration))

    def train_test(self):
        x_in, y_in = self.data_loader.test_data()

        for epoch in range(0, self.max_step):
            fetch_list = [self.summary_op, self.D_train_step, self.G_train_step,
                          self.G_loss, self.D_loss, self.G, self.D, self.step_increment]

            feed_dict = {self.x : x_in, self.y : y_in}
            summary, _, _, g_loss, d_loss, G, D, _ = self.sess.run(fetch_list, feed_dict)
            self.summary_writer.add_summary(summary, self.step.eval(session=self.sess))

            if epoch % self.log_step == 0:
                print "G_loss: ", g_loss
                print "D_loss: ", d_loss
                print "--------------------------"
